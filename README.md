# Freedom Privacy over Internet

Welcome to Freedom Privacy over Internet (FPoI) project about how to maintain freedom of speech and privacy using technology and the Internet.<br/>
How to protected against mass surveillance programs implemented by the states and how to live without Google, Apple, Facebook, Amazon, Microsoft (GAFAM) and ICT corporations.<br/>
This project is an effort to improve people awareness about states mass surveillance and ICT corporations influence on their life.

## Note
This is a community project that provides information for protecting your freedom of speech and your privacy. Using the recommended projects listed on this Web site does not guarantee that 100% of your communications will be protected against mass surveillance programs implemented by the states.<br/>
Please never trust any company, always encrypt and perform your own search before trusting these projects and using them with sensitive information.

# Contributing

This is a community project which aim is to deliver the best information available for your freedom of speech and your privacy.<br/>
Thank you for participating with suggestions and constructive criticism.<br/>
It is important to maintain up to date material. We keep an eye on software updates of the applications described into the guide.<br/>
We try our best to keep up, but we are not perfect and the Internet changes very fast. So if you find an error, or you think that a service or application should not be listed, please contact us.

# Project Contribution Guidelines

## FOSS software and OSS can be featured on FPoI

FPoI follows the GNU/FSF definition of <a href="https://en.wikipedia.org/wiki/The_Free_Software_Definition">Free Software</a>. GPL-like software are preferred over other open source OSI approved licenses. Exceptions are only allowed when GPL-like solutions are not available.
<a href="https://en.wikipedia.org/wiki/Software_license">FOSS license</a>:
1. Copyleft: GPL v2+, AGPL v2+, CC-BY-SA v3+
2. Weak copyleft: LGPL v2+, MPL v2
3. Permissive: Apache v2, MIT, X11, BSD 3-clause

## Quality over quantity

FPoI tries to promote the best FOSS and OSS applications.
- User friendly
- Stable
- Fast
- Security focus (well known crypto and security algorithms, updates frequency, documentation, etc.)
- Privacy oriented (e2e encryption, no or minimum metadata leaks, company's stance on customers' privacy, etc.)

People are looking for a valid alternative to leave their proprietary walled gardens (GAFAM).

## Before suggesting software, please first read properly the material to find out if your request has already been made

If it has already been rejected, you will understand why. Otherwise, please add a comment to describe why it deserves inclusion.
# TO DO

- [x] English / English looking for contributors
- [x] Italiano / Italian looking for contributors
- [x] Español / Spanish looking for contributors
- [x] Web site: ~~Wordpress~~ or ~~Jekyll~~? Publii ;)

# License

- <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPLv3</a>.
- <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">CC-BY-SA v4.0</a>.